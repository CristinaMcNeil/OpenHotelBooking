## Open Hotel Booking

Booking for an hotel has now made easier and faster. If you are tired of looking for the one that suite your budget and preference, this is not the same thing now. With some clicks online, you can see what the hotel will look like and how much will be a night of stay with them. And that made possible because of online booking.

Check out this simple flow of how an online booking works. You can find more of these [here](https://www.psychz.net/dedicated-hosting.html).

### Setting up
#### Run migrations
```
php artisan migrate
```

#### Seed the database
```
php artisan db:seed
```

#### Configuration
Edit configuration files in `app/config` directory.

### Admin Login
Go to `/admin/login` and enter the following credentials:<br>
Email: admin@example.com<br>
Password: 12345678<br>